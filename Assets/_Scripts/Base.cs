﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Base : MonoBehaviour
{
    Rigidbody rb;
    Vector3 movementVector;
    Vector3 inputVector;
    float speed;
    float baseSpeed = 2f;
    float boostSpeed = 6f;
    
    [SerializeField]
    Transform top;
	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKey(KeyCode.LeftShift))
        {
            speed = boostSpeed;
        }
        else
        {
            speed = baseSpeed;
        }
        inputVector = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        movementVector = top.TransformDirection(inputVector);
        
        if (inputVector != Vector3.zero)
        {
            
            if(Quaternion.Angle(transform.rotation, Quaternion.LookRotation(movementVector)) < 90f)
            {
               
                rb.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movementVector), speed * Time.deltaTime));
                rb.MovePosition(transform.position + transform.forward * speed * Time.deltaTime);
            }
            else
            {
                rb.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(-movementVector), speed * Time.deltaTime));
                rb.MovePosition(transform.position - transform.forward * speed * Time.deltaTime);
            }
            // This means the vehicle does not move directly towards target until the rotation has finished happening ie 'turn circle'
        }
        
        
    }
}
