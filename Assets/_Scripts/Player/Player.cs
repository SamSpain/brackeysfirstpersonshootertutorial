﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerSetup))]
[RequireComponent(typeof(Collider))]
public class Player : NetworkBehaviour
{
    [SyncVar]
    private bool isDead = false;
    public bool IsDead
    {
        get { return isDead; }
        protected set { isDead = value; }
    }

    [SerializeField]
    private int maxHealth = 100;
    [SyncVar]
    private int currentHealth;

    [SerializeField]
    private Behaviour[] disableOnDeath;
    private bool[] wasEnabled;

    [SerializeField]
    private GameObject[] disableGameObjectsOnDeath;

    [SerializeField]
    private GameObject deathEffect;

    [SerializeField]
    private GameObject spawnEffect;

    private bool firstSetup = true;



    void Update()
    {
        if (!isLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.K))
        {
            RpcTakeDamage(1000);
        }
    }
    public void SetupPlayer()
    {
        if(isLocalPlayer)
        {

            // switch camera
            
            GameManager.instance.SetSceneCameraActive(false);
            GetComponent<PlayerSetup>().playerUIInstance.SetActive(true);
        }
        

        CmdBroadcastNewPlayerSetup();
    }

    [Command]
    private void CmdBroadcastNewPlayerSetup()
    {
        RpcSetupPlayerOnAllClients();
    }

    [ClientRpc]
    private void RpcSetupPlayerOnAllClients()
    {
        if(firstSetup)
        {
            wasEnabled = new bool[disableOnDeath.Length];
            for (int i = 0; i < wasEnabled.Length; i++)
            {
                wasEnabled[i] = disableOnDeath[i].enabled;
            }

            firstSetup = false;
        }
        
        SetDefaults();
    }

    public void SetDefaults()
    {
        IsDead = false;
        currentHealth = maxHealth;

        // Enable the components
        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            disableOnDeath[i].enabled = wasEnabled[i];
        }

        // Enable the gameobjects
        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(true);
        }

        // enable the collider
        Collider collider = GetComponent<Collider>();
        if(collider != null)
        {
            collider.enabled = true;
        }

        
        // spawn effect
        GameObject graphicsEffectsInstance = (GameObject)Instantiate(spawnEffect, transform.position, Quaternion.identity);
        Destroy(graphicsEffectsInstance, 3f);


    }

    [ClientRpc]
    public void RpcTakeDamage(int weaponDamage)
    {
        if(isDead)
        {
            return;
        }
        currentHealth -= weaponDamage;
        Debug.Log(transform.name + " now has " + currentHealth + " HP.");

        if(currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        IsDead = true;

        // disable components
        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            disableOnDeath[i].enabled = false;
        }
        // disable game objects
        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(false);
        }


        Debug.Log(transform.name + " died.");

        // disable collider
        Collider collider = GetComponent<Collider>();
        if (collider != null)
        {
            collider.enabled = true;
        }
        // spawn death effect
        GameObject graphicsEffectsInstance = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(graphicsEffectsInstance, 3f);

        // switch camera
        if(isLocalPlayer)
        {
            GameManager.instance.SetSceneCameraActive(true);
            GetComponent<PlayerSetup>().playerUIInstance.SetActive(false);
        }
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(GameManager.instance.matchSettings.respawnTime);
        
        transform.position = NetworkManager.singleton.GetStartPosition().position;
        transform.rotation = NetworkManager.singleton.GetStartPosition().rotation;
        yield return new WaitForSeconds(0.1f);
        SetupPlayer();
        Debug.Log(transform.name + " respawned");
    }
}
