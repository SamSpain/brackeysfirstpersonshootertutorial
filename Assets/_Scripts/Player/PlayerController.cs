﻿using UnityEngine;
[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(PlayerMotor))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float
    speed = 5f,
    lookSensitivity = 3f,
    thrusterForce = 1000f,
    thrusterFuelBurnSpeed = 1f,
    thrusterFuelRegenSpeed = .3f,
    thrusterFuelAmount = 1f;


    [SerializeField]
    private LayerMask environmentMask;
    

    [Header("Spring settings:")]
    
    [SerializeField]
    private float jointSpring = 20f;
    [SerializeField]
    private float jointMaxForce = 40f;

    private PlayerMotor motor;
    private ConfigurableJoint joint;
    private Animator animator;

    void Start()
    {
        motor = GetComponent<PlayerMotor>();
        joint = GetComponent<ConfigurableJoint>();
        animator = GetComponent<Animator>();
        SetJointSettings(jointSpring);

    }

    void Update()
    {
        // setting target position for string
        // this makes the physics act right when it comes to applying
        // gravity when flying
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 100f, environmentMask))
        {
            joint.targetPosition = new Vector3(0f, -hit.point.y, 0f);
        }
        else
        {
            joint.targetPosition = Vector3.zero;
        }


        // calculate movement velocity as a 3D vector
        float xMovement = Input.GetAxis("Horizontal");
        float zMovement = Input.GetAxis("Vertical");

        Vector3 movementHorizontal = transform.right * xMovement;
        Vector3 movementVertical = transform.forward * zMovement;

        // Final movememnt  vector
        Vector3 velocity = (movementHorizontal + movementVertical) * speed;

        // animate movement
        animator.SetFloat("ForwardVelocity", zMovement);

        // Apply movement 
        motor.Move(velocity);

        // Calculate rotation as a 3D vector (turning around)
        float yRotation = Input.GetAxisRaw("Mouse X");
        Vector3 rotation = new Vector3(0f, yRotation, 0f) * lookSensitivity;

        // Apply rotation
        motor.Rotate(rotation);
        // we only want the camera to rotate up and down


        // Calculate rotation as a 3D vector (turning around)
        float xRotation = Input.GetAxisRaw("Mouse Y");
        float cameraRotationX = xRotation * lookSensitivity;

        // Apply rotation
        motor.CanmeraRotate(-cameraRotationX);


        Vector3 thrusterForceVector = Vector3.zero;

        // Apply thruster force
        if (Input.GetButton("Jump")&& thrusterFuelAmount > 0f)
        {
            thrusterFuelAmount -= thrusterFuelBurnSpeed * Time.deltaTime;

            if (thrusterFuelAmount >= float.Epsilon)
            {
                thrusterForceVector = Vector3.up * thrusterForce;
                SetJointSettings(0f);
            }
        }
        else
        {
            thrusterFuelAmount += thrusterFuelRegenSpeed * Time.deltaTime;
            SetJointSettings(jointSpring);
        }
        thrusterFuelAmount = Mathf.Clamp(thrusterFuelAmount, 0, 1);
        motor.ApplyThrusterForce(thrusterForceVector);
            

        


    }

    public float GetThrusterFuelAmount()
    {
        return thrusterFuelAmount;
    }

    private void SetJointSettings(float jointSpring)
    {
        joint.yDrive = new JointDrive
        {
            
            positionSpring = jointSpring,
            maximumForce = jointMaxForce
        };
    }



}
