﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(NetworkIdentity))]
[RequireComponent(typeof(PlayerController))]
public class PlayerSetup : NetworkBehaviour
{
    [SerializeField]
    Behaviour[] componentsToDisable;

    [SerializeField]
    string remoteLayerName = "RemotePlayer";

    [SerializeField]
    string dontDrawLayerName = "DontDraw";
    [SerializeField]
    GameObject playerGraphics;
    [SerializeField]
    private GameObject playerUIPrefab;
    [HideInInspector]
    public GameObject playerUIInstance;


    Camera sceneCamera;
    void Start()
    {
        if (!isLocalPlayer) // this object is not the local player
        {
            DisableComponents();
            AssignRemoteLayer();
        }
        else
        {
            

            // Disable player graphics for local player
            SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));

            // create player UI
            playerUIInstance = Instantiate(playerUIPrefab);
            playerUIInstance.name = playerUIPrefab.name;

            // configure playerUI
            PlayerUI playerUI = playerUIInstance.GetComponent<PlayerUI>();
            if(playerUI == null)
            {
                Debug.LogError("No UI component found for " + transform.name);
            }
            playerUI.SetController(GetComponent<PlayerController>());
            GetComponent<Player>().SetupPlayer();
        }

        
        

    }

    private void SetLayerRecursively(GameObject obj, int layer)
    {
        obj.layer = layer;
        foreach(Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, layer);
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        GameManager.RegisterPlayer(GetComponent<NetworkIdentity>().netId.ToString(), GetComponent<Player>());
    }

    void DisableComponents()
    {
        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsToDisable[i].enabled = false;
        }
    }
    void AssignRemoteLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
    }

    void OnDisable() //Called when destroyed
    {
        Destroy(playerUIInstance);
        if (isLocalPlayer)
        {
            GameManager.instance.SetSceneCameraActive(true);
        }
        

        GameManager.DeRegisterPlayer(transform.name);
    }
}
