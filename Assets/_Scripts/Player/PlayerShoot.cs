﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
[RequireComponent(typeof(WeaponManager))]
public class PlayerShoot : NetworkBehaviour
{
    private const string PLAYER_TAG = "Player";
    private WeaponManager weaponManager;
    private PlayerWeapon currentWeapon;
    
    
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private LayerMask mask;

	// Use this for initialization
	void Start ()
    {
	    if(cam == null)
        {
            Debug.LogError("PlayerShoot: No camera referenced.");
            this.enabled = false;
        }
        weaponManager = GetComponent<WeaponManager>();
        
	}
	
	// Update is called once per frame
	void Update ()
    {

        currentWeapon = weaponManager.GetCurrentWeapon();
        if(currentWeapon.fireRate <= 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        else
        {
            if(Input.GetButtonDown("Fire1"))
            {
                InvokeRepeating("Shoot", 0f, 1f/currentWeapon.fireRate);
            }
            else if(Input.GetButtonUp("Fire1"))
            {
                CancelInvoke("Shoot");
            }
                 
        }
        

	}
    // is called on the server when the player shoots
    [Command]
    void CmdOnShoot()
    {
        RpcDoShootEffect();
    }

    // is called on all clients when we need to do a shoot effect
    [ClientRpc]
    void RpcDoShootEffect()
    {
        weaponManager.GetCurrentGraphics().muzzleFlash.Play();
    }

    // is called on the server when we hit something
    // takes in the hit point and the normal of the surface
    [Command]
    void CmdOnHit(Vector3 position, Vector3 normal)
    {
        RpcDoHitEffect(position, normal);
    }

    // is called on all clients
    // here we can spawn effect
    [ClientRpc]
    void RpcDoHitEffect(Vector3 position, Vector3 normal)
    {
        GameObject hitEffect = (GameObject)Instantiate(weaponManager.GetCurrentGraphics().hitEffectPrefab, position, Quaternion.LookRotation(normal));
        Destroy(hitEffect, 2f);
    }

    [Client]
    void Shoot()
    {
        if(!isLocalPlayer)
        {
            return;
        }
        // we are shooting, call the onshoot method on the server
        CmdOnShoot();
        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask))
        {
            if(hit.collider.tag == PLAYER_TAG)
            {
                CmdPlayerShot(hit.collider.name, currentWeapon.damage);
            }

            // we hit something, call the OnHit method on the server
            CmdOnHit(hit.point, hit.normal);
            //Debug.Log(transform.name + " fired at " + hit.transform.name);
        }

    }

    [Command]
    void CmdPlayerShot(string playerID, int weaponDamage)
    {
        Debug.Log(playerID + " has been shot.");

        GameManager.GetPlayer(playerID).RpcTakeDamage(weaponDamage);
    }

}
