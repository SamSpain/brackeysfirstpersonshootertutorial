﻿using UnityEngine;
using System.Collections;

public class PlayerUI : MonoBehaviour {

    [SerializeField]
    RectTransform thrusterFuelFill;
    private PlayerController controller;

    void SetFuelAmount(float amount)
    {
        thrusterFuelFill.localScale = new Vector3(1, amount, 1);
    }

    public void SetController(PlayerController controller)
    {
        this.controller = controller;
    }

    void Update()
    {
        SetFuelAmount(controller.GetThrusterFuelAmount());
    }
}
