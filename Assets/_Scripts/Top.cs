﻿using UnityEngine;
using System.Collections;

public class Top : MonoBehaviour
{
    
    Quaternion rotation;
    float mouseMovement;
	// Use this for initialization
	void Start ()
    {
        rotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update ()
    {
        mouseMovement += Input.GetAxisRaw("Mouse X");
        rotation.eulerAngles = new Vector3(0f, mouseMovement, 0f); // we only ever want to rotate around the y axis locally!
        
    }

    void LateUpdate()
    {
        transform.localRotation = rotation; // changes the local rotation of the turret
        
        
        

    }
}
